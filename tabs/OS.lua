Text = class()
Window = class()

local os_skin = readImage("Dropbox:os_skin")
local os_font = readImage("Dropbox:os_font")
local os_font_string = " abcdefghijklmnopqrstuvwxyzäöüß1234567890.:,;!?/|\\'\"#%*+-_~=[](){}<>^$"
local os_font_letter_size = vec2(3, 5)

local function Template(width, height, regions)
    local tile_size = 4
    local img = image(width, height)
    local tpl = {
        header_left   = uvTexture(os_skin, tile_size, tile_size, regions[1],  regions[2],  true),
        header_center = uvTexture(os_skin, tile_size, tile_size, regions[3],  regions[4],  true),
        header_right  = uvTexture(os_skin, tile_size, tile_size, regions[5],  regions[6],  true),
        body_left     = uvTexture(os_skin, tile_size, tile_size, regions[7],  regions[8],  true),
        body_center   = uvTexture(os_skin, tile_size, tile_size, regions[9],  regions[10], true),
        body_right    = uvTexture(os_skin, tile_size, tile_size, regions[11], regions[12], true),
        footer_left   = uvTexture(os_skin, tile_size, tile_size, regions[13], regions[14], true),
        footer_center = uvTexture(os_skin, tile_size, tile_size, regions[15], regions[16], true),
        footer_right  = uvTexture(os_skin, tile_size, tile_size, regions[17], regions[18], true)
    }
    
    for part, prop in pairs(tpl) do
        local size = vec2(prop.width, prop.height)
        local pos = size/2
        
        if part:find("header") then
            pos.y = math.floor(height - size.y/2)
        end
        if part:find("body") then
            size.y = height - tpl.footer_center.height - tpl.header_center.height
            pos.y = tpl.footer_center.height + size.y/2
        end
        if part:find("center") then
            size.x = width - tpl.body_left.width - tpl.body_right.width
            pos.x = tpl.body_left.width + size.x/2
        end
        if part:find("right") then
            pos.x = math.floor(width - size.x/2)
        end
        
        local region = mesh()
        region.texture = os_skin
        region:addRect(pos.x, pos.y, size.x, size.y)
        region:setRectTex(1, prop.uv.x1, prop.uv.y1, prop.uv.w, prop.uv.h)
        
        pushStyle()
        noSmooth()
        setContext(img)
        region:draw()
        setContext()
        popStyle()
    end
    
    local render = mesh()
    render.texture = img
    render:addRect(width/2, height/2, width, height)
    render:setRectTex(1, 0, 0, 1, 1)
    render.x = 0
    render.y = 0
    render.width = width
    render.height = height
    render.regions = regions
    
    return render
end

function Text:init(string_value, display_type)
    self.string = string_value
    self.type = display_type
    
    self.type(self)
end

function Text:test()
    print("aha got you")
    print(self.string)
end

function Text:draw()
end

function Window:init(x, y, width, height, title, content)
    self.x = x or WIDTH/4
    self.y = y or HEIGHT/3
    self.width = width or WIDTH/2
    self.height = height or HEIGHT/3
    self.min_width = 128
    self.min_height = 128
    self.scale = 2
    self.color = colorPico8.white
    self.title = title
    self.content = content
    
    self.frame_shell = Template(self.width / self.scale, self.height / self.scale, {
        vec2(7, 4), vec2(7, 4), vec2(8, 4), vec2(8, 4), vec2(9, 4), vec2(9, 4),
        vec2(7, 5), vec2(7, 5), vec2(8, 5), vec2(8, 5), vec2(9, 5), vec2(9, 5),
        vec2(7, 6), vec2(7, 6), vec2(8, 6), vec2(8, 6), vec2(9, 6), vec2(9, 6)
    })
    
    if self.content then
        self.content_inlay = Template(self.frame_shell.width - 4, self.frame_shell.height - 4, {
            vec2(10, 10), vec2(10, 10), vec2(11, 10), vec2(11, 10), vec2(12, 10), vec2(12, 10),
            vec2(10, 11), vec2(10, 11), vec2(11, 11), vec2(11, 11), vec2(12, 11), vec2(12, 11),
            vec2(10, 12), vec2(10, 12), vec2(11, 12), vec2(11, 12), vec2(12, 12), vec2(12, 12)
        })
    
        self.content_inlay.x = 2
        self.content_inlay.y = 2
    else
        self.content_blanko = Template(self.frame_shell.width - 4, self.frame_shell.height - 4, {
            vec2(7, 7), vec2(7, 7), vec2(8, 7), vec2(8, 7), vec2(9, 7), vec2(9, 7),
            vec2(7, 8), vec2(7, 8), vec2(8, 8), vec2(8, 8), vec2(9, 8), vec2(9, 8),
            vec2(7, 9), vec2(7, 9), vec2(8, 9), vec2(8, 9), vec2(9, 9), vec2(9, 9)
        })
        
        self.content_blanko.x = 2
        self.content_blanko.y = 2
    end
    
    self.header = Template(self.frame_shell.width - 4, 16, {
        vec2(7, 1), vec2(7, 1), vec2(8, 1), vec2(8, 1), vec2(9, 1), vec2(9, 1),
        vec2(7, 2), vec2(7, 2), vec2(8, 2), vec2(8, 2), vec2(9, 2), vec2(9, 2),
        vec2(7, 3), vec2(7, 3), vec2(8, 3), vec2(8, 3), vec2(9, 3), vec2(9, 3)
    })
    
    self.header.x = 2
    self.header.y = self.frame_shell.height - 18
    self.header.static_height = true
    
    self.parts = { -- draw in order
        "frame_shell",
        self.content and "content_inlay" or "content_blanko",
        "header"
    }
end

function Window:draw()
    pushMatrix()
    translate(self.x, self.y)
    scale(self.scale)
    for _, part_name in ipairs(self.parts) do
        pushMatrix()
        translate(self[part_name].x, self[part_name].y)
        self[part_name]:setColors(self.color)
        self[part_name]:draw()
        popMatrix()
    end
    popMatrix()
end

function Window:rerender(delta_width, delta_height)
   for _, part_name in ipairs(self.parts) do
        local part = self[part_name]
        local static_w = part.static_width
        local static_h = part.static_height
        local w = static_w and part.width or (part.width + delta_width)
        local h = static_h and part.height or (part.height + delta_height)
        local x = part.x --static_w and (part.x - delta_height) or part.x
        local y = static_h and (part.y + delta_height) or part.y
        
        self[part_name] = Template(w, h, part.regions)
        self[part_name].x = x
        self[part_name].y = y
        self[part_name].static_width = static_w
        self[part_name].static_height = static_h
    end
end

function Window:orientationChanged(screen)
    if screen.prevOrientationName ~= screen.currOrientationName then
        local rel_x, rel_y = pnt_rel(self.x, self.y, screen.prevWidth, screen.prevHeight)
        local rel_w, rel_h = pnt_rel(self.width, self.height, screen.prevWidth, screen.prevHeight)
        local new_x, new_y = pnt_abs(rel_x, rel_y, screen.currWidth, screen.currHeight)
        local new_w, new_h = pnt_abs(rel_w, rel_h, screen.currWidth, screen.currHeight)
        local delta_w = (new_w - self.width) / self.scale
        local delta_h = (new_h - self.height) / self.scale
        
        self.x = new_x
        self.y = new_y
        self.width = new_w
        self.height = new_h
        self:rerender(delta_w, delta_h)
    end
end

function Window:touched(touch)
    local touch_x = (touch.x - self.x) / self.scale
    local touch_y = (touch.y - self.y) / self.scale
    
    if touch.state == BEGAN then
        if touch.x > self.x and touch.x < self.x + self.width -- touch inside window
        and touch.y > self.y and touch.y < self.y + self.height
        then
            if touch_y > self.header.y and touch_y < self.header.y + self.header.height then -- touch inside header
                self.is_moving = true
            elseif (touch.x > self.x and touch.x < self.x + self.scale * 8) -- touched left or right side
            or (touch.x > self.x + self.width - self.scale * 8 and touch.x < self.x + self.width)
            then
                self.is_resizing_width = true -- global status accessor
                if touch.x < self.x + self.width/2 then
                    self.is_resizing_width_left = true -- resize to the right
                else
                    self.is_resizing_width_right = true -- resize to the left
                end
            elseif (touch.y > self.y and touch.y < self.y + self.scale * 8) then -- touched bottom
                self.is_resizing_height = true
            end
        end
    end
    
    if touch.state == MOVING then
        if self.is_moving then -- move window
            self.x = self.x + touch.deltaX
            self.y = self.y + touch.deltaY
        elseif self.is_resizing_width then -- resize window
            if self.is_resizing_width_right then -- resize to the right
                local delta = touch.deltaX
                local width = self.width + delta
                if width > self.min_width then
                    self.width = width
                    self:rerender(delta / self.scale, 0)
                end
            elseif self.is_resizing_width_left then -- resize to the left
                local delta = -touch.deltaX
                local width = self.width + delta
                if width > self.min_width then
                    self.x = self.x - delta
                    self.width = width
                    self:rerender(delta / self.scale, 0)
                end
            end
        elseif self.is_resizing_height then -- resize to the bottom
            local delta = -touch.deltaY
            local height = self.height + delta
            if height > self.min_height then
                self.y = self.y - delta
                self.height = height
                self:rerender(0, delta / self.scale)
            end
        end
    end
    
    if touch.state == ENDED then
        self.is_moving = nil
        self.is_resizing_width = nil
        self.is_resizing_width_left = nil
        self.is_resizing_width_right = nil
        self.is_resizing_height = nil
    end
end
