Stream = class()

function Stream:init()
    self.size = math.min(WIDTH, HEIGHT) / 3
    self.num_strips = 8
    self.colors = self:gen_colors()
    self.angle = 0
    
    parameter.integer("stream_break_timer", 0, 60, 1, function(time)
        local function countdown()
            self.remaining = stream_break_timer
            
            if self.countdown then tween.reset(self.countdown) end
            
            self.countdown = tween(60, self, {angle = self.angle - 360}, tween.easing.linear, function()
                stream_break_timer = stream_break_timer - 1
                self.remaining = stream_break_timer
                
                if self.remaining > 0 then
                    countdown()
                end
            end)
        end
        countdown()
    end)
end

function Stream:gen_colors()
    local whitelist = {}
    local strip_colors = {
        colorPico8.dark_blue,
        colorPico8.dark_purple,
        colorPico8.dark_green,
        colorPico8.brown,
        colorPico8.dark_gray,
        colorPico8.light_gray,
        colorPico8.red,
        colorPico8.orange,
        colorPico8.yellow,
        colorPico8.green,
        colorPico8.blue,
        colorPico8.indigo,
        colorPico8.pink,
        colorPico8.peach
    }
    for s = 1, self.num_strips do
        local color_id repeat color_id = math.random(#strip_colors) until not whitelist[color_id]
        whitelist[color_id] = strip_colors[color_id]
    end
    return whitelist
end

function Stream:draw()
    if stream_break_timer > 0 then
        pushMatrix()
        pushStyle()
        
        self.size = math.min(WIDTH, HEIGHT) / 3
        
        if not self.time or self.time <= ElapsedTime then
            self.time = ElapsedTime + 1
            self.colors = self:gen_colors()
        end
        
        do local i = 0
            for id, col in pairs(self.colors) do
                i = i + 1
                local w = WIDTH / self.num_strips
                local h = HEIGHT
                fill(col)
                rectMode(CORNER)
                rect(w*i-w, 0, w, h)
            end
        end
        
        noStroke()
        fill(colorPico8.black)
        font("Futura-CondensedExtraBold")
        
        fontSize(16)
        text(string.upper(string.format("stream continues in %d minutes", self.remaining)), WIDTH/2, HEIGHT - 50)
        
        translate(WIDTH/2, HEIGHT/2)
        
        fontSize(48)
        text(self.remaining - 1, 0, self.size/2)
        fontSize(24)
        text(self.remaining - .25, self.size/2, 0)
        text(self.remaining - .5, 0, -self.size/2)
        text(self.remaining - .75, -self.size/2, 0)
        
        pushMatrix()
        rotate(self.angle)
        rectMode(CORNER)
        rect(-2, 0, 4, self.size/2 - 50) -- cursor
        ellipse(0, 0, 10)
        popMatrix()
        
        noFill()
        stroke(colorPico8.black)
        strokeWidth(2)
        line(-self.size/2, -self.size/2, self.size/2, self.size/2)
        line(self.size/2, -self.size/2, -self.size/2, self.size/2)
        
        popStyle()
        popMatrix()
    end
end
