-- startup

displayMode(STANDARD)

function setup()
    parameter.watch("EngineProfile")
    stream_break = Stream()
    
    --w = Window(WIDTH-400, HEIGHT-200, 400, 200)
    w = Window(0, 0)
    --w.color = colorPico8.peach
    
    Text("test", Text.test)
    
    --[[
    local hour_overhead = 5.5 -- hours
    local second_overhead = hour_overhead * 60 * 60
    math.randomseed(os.time() + second_overhead)
    local fiveOfFifty = {}
    for i = 1, 5 do
        table.insert(fiveOfFifty, math.random(50))
    end
    print("Zahlen:", table.concat(fiveOfFifty, ", "))
    print("Zusatz:", math.random(10), math.random(10))
    --]]
end

function orientationChanged(screen)
    if w then w:orientationChanged(screen) end
end

function draw()
    noSmooth()
    background(colorPico8.indigo)
    
    if stream_break.remaining > 0 then
        stream_break:draw()
    else
        if w then w:draw() end
    end
    
    do EngineProfile = string.format("Frame Rate: %.2fms \nUpdate Frequency: %ifps \nLua Memory: %.0fkb",
        1000 * DeltaTime,
        math.floor(1/DeltaTime),
        collectgarbage("count"))
        collectgarbage()
    end
end

function touched(touch)
    if w then w:touched(touch) end
end
